import './App.css';
import 'boxicons'
import Header from './components/Header/Header'
import Home from './components/Home/Home.jsx'

function App() {
  return (
    <div className="App">
      <Header/>
      <Home/>
    </div>
  );
}

export default App;
