import React from "react"

const Social = () => {
    return (
        <div className="home__social">
            <a href="https://www.instagram.com/nuyen.s99/" className="home__social-icon" target="_blank">
                <i className="uil uil-instagram"></i>
            </a>

            <a href="https://www.facebook.com/profile.php?id=100019798788321" className="home__social-icon" target="_blank">
                <i className="uil uil-facebook-f"></i>
            </a>

            <a href="" className="home__social-icon" target="_blank">
                <i className="uil uil-gitlab"></i>
            </a>
        </div>
    )
}

export default Social